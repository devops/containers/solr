ARG solr_version=7
FROM solr:${solr_version}

# Step up to root
USER 0

RUN set -eux; \
	apt-get -y update; \
	apt-get -y upgrade; \
	apt-get -y install \
	less \
	locales \
	locales-all \
	nano \
	; \
	apt-get -y clean

ENV LANG     en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL   en_US.UTF-8
ENV TZ       US/Eastern

RUN locale-gen $LANG

HEALTHCHECK CMD curl -f http://localhost:8983/solr

# Step back down to solr
USER $SOLR_USER
