#!/bin/bash

source .env
cid="$(docker run --rm -d $build_tag)"
code=1

SECONDS=0
while [[ $SECONDS -lt 60 ]]; do
    echo -n "Checking health status ... "
    status=$(docker inspect -f '{{.State.Health.Status}}' $cid)
    echo $status
    case $status in
        healthy)
	    code=0
            break
            ;;
        unhealthy)
            break
            ;;
        starting)
            sleep 5
            ;;
        *)
            echo "Unexpected status."
            break
            ;;
    esac
done

docker stop $cid
exit $code
