#!/bin/bash

source .env
docker build --pull --build-arg "solr_version=${solr_version}" \
       -t "${build_tag}" - \
       < Dockerfile
